#!/usr/bin/python
import math
import argparse
from ast import literal_eval as make_tuple

# Based alot on https://gist.github.com/JoanTheSpark/e5afd7081d9d1b9ad91a
# Removed some unused code, added argparse to generate.
# No longer creating a trace for pcbnew, but a footprint
# Changed alot of paramters. Round_spiral is mostly what remains

def Round_spiral (radius,
                segs,
                startangle,
                trackWidth,
                trackSpace,
		resolution,
                turns,
                spin,
                layer,
                ):

    baseX = 0.0
    baseY = 0.0
    
    STR_data = []

    # when calculated, trackspace must be the total space taken by both one track and its space
    trackSpace = trackWidth + trackSpace

    for j in range(turns):

        segs += resolution
        segangle = 360.0 / segs
        segradius = trackSpace / segs

        for i in range(int(segs)):
            # central rings for HV and SNS
            endX = str(baseX + (radius + segradius * i + trackSpace * (j+1)) * math.sin(math.radians(segangle*spin*i + startangle)))
            endY = str(baseY + (radius + segradius * i + trackSpace * (j+1)) * math.cos(math.radians(segangle*spin*i + startangle)))
            startX = str(baseX + (radius + segradius * (i + 1.0) + trackSpace * (j+1)) * math.sin(math.radians(segangle*spin*(i + 1.0) + startangle)))
            startY = str(baseY + (radius + segradius * (i + 1.0) + trackSpace * (j+1)) * math.cos(math.radians(segangle*spin*(i + 1.0) + startangle)))
	    
            STR_data.append("(fp_line (start "+startX+" "+startY+") (end "+endX+" "+endY+") (layer "+layer+") (width "+str(trackWidth)+"))")
    return STR_data

def line2coord(line, divider):
    split = line.split(divider+" ") 
    coord = split[1].split(")", 1)[0]
    return coord

def coord2tuple(coord):
    coord = coord.replace(' ', ',')
    return make_tuple(coord)

def getPad(line, divider, trackWidth, layer, padNo):
    coord = line2coord(line, divider)
    padNo = str(padNo)
    padString = "(pad "+padNo+" smd rect (at "+coord+") (size "+str(trackWidth)+" "+str(trackWidth)+") (layers "+layer+"))"
    return padString

def getVia(line, divider):
    coord = line2coord(line, divider)
    viaString = " (pad \"\" thru_hole circle (at "+coord+") (size 0.40 0.40) (drill 0.30) (layers *.Cu *.Mask F.SilkS) (zone_connect 2))"
    return viaString
    
def createCoil(Sides, TrackWidth, TrackDistance, Radius, CenterVia,  Resolution, Via, Drill, Name, NoOfLayers, Turns):
    Center = [0.0,0.0] # x/y coordinates of the centre of the pcb sheet
    #Radius = 2.5 # start radius in mm
    #Sides = 20.0
    StartAngle = 0.0 # degrees
    #TrackWidth = 0.1016
    #TrackDistance = 0.1016
    #Turns = 40
    Spin = -1 # ccw=1 cw=-1
    #NoOfLayers = 2
    #Name = "test"

    header = [
	"(module \""+Name+"\" (layer F.Cu) (tedit 55A681D4)",
	"\t(fp_text reference G*** (at 0 0) (layer F.SilkS) hide",
	"\t  (effects (font (thickness 0.3)))",
	"\t)",
	"\t(fp_text value \""+Name+"\" (at 0 -2.54) (layer F.SilkS) hide",
	"\t  (effects (font (thickness 0.3)))",
	"\t)" ]

    footer = [ ')' ]


    spiral_1 = list(reversed(Round_spiral(Radius, Sides, StartAngle, TrackWidth, TrackDistance, Resolution, Turns, Spin, "F.Cu")))
    spiral_2 = Round_spiral(Radius, Sides, StartAngle, TrackWidth, TrackDistance, Resolution, Turns, Spin*-1, "B.Cu")

    if CenterVia:
	    #STR_data.append("(fp_line (start "+startX+" "+startY+") (end "+endX+" "+endY+") (layer "+layer+") (width "+str(trackWidth)+"))")
      spiral_1.append("(fp_line (start "+line2coord(spiral_1[len(spiral_1)-1], "end")+") (end 0.0 0.0) (layer F.Cu) (width "+str(TrackWidth)+"))")
      spiral_2.insert(0, "(fp_line (start "+line2coord(spiral_2[0], "end")+") (end 0.0 0.0) (layer B.Cu) (width "+str(TrackWidth)+"))")

    # from spiral 2, remove the last element(s) to have pads at different points. need atleast trackSpace distance
    def distance():
      s2 = coord2tuple(line2coord(spiral_2[len(spiral_2)-1], "start"))
      s1 = coord2tuple(line2coord(spiral_1[0], "start"))
      return math.sqrt(math.pow(float(s2[0])-float(s1[0]), 2) + math.pow(float(s2[1])-float(s1[1]), 2))
    while(distance()<TrackDistance*2):
      spiral_2 = spiral_2[:len(spiral_2)-1]

    total = []
    if NoOfLayers == 1:
      padStart = getPad(spiral_1[0], "end", TrackWidth, "F.Cu", 1)
      padEnd = getPad(spiral_1[len(spiral_1)-1], "end", TrackWidth, "F.Cu", 2)

      total.extend(header)
      total.append(padStart)
      total.extend(spiral_1)
      total.append(padEnd)

    if NoOfLayers == 2:
      padStart = getPad(spiral_1[0], "start", TrackWidth, "F.Cu", 1)
      # spiral_2 has reversed elements. so get start coord instead of end coord
      padEnd = getPad(spiral_2[len(spiral_2)-1], "start", TrackWidth, "B.Cu", 2)
      # and same for via, but opposite
      via = getVia(spiral_2[0], "end")

      total.extend(header)
      total.append(padStart)
      total.extend(spiral_1)
      total.append(via)
      total.extend(spiral_2)
      total.append(padEnd)


    total.extend(footer)
    for line in total:
      print(line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="""Generates single or double sided round board coils for use in kicad as footprints.
PS! The use of pad for via is yet untested.""")
    parser.add_argument('-d', dest='sides', type=int, default=2, 
                        help="Number of layers used for coil, (max=2), default=2")
    parser.add_argument('-lw', dest='linewidth', type=float, default=0.3,
                        help="Linewidth, default=0.3")
    parser.add_argument('-sp', dest='spacing', type=float, default=0.3, 
                        help="spacing, default=0.3")
    parser.add_argument('-x', dest='resolution', type=float, default=16, 
                        help="resolution of points along the spiral. Higher is higher resolution (and bigger file), default=16")
    parser.add_argument('-r', '--radius', dest='radius', type=float, default=2.5, 
                        help="Inner radius (mm), default=2.5")
    parser.add_argument('-centerVia', dest='centerVia', action='store_true',
                        help="Use -centerVia if you want the via to be in the coil centre")
    parser.set_defaults(centerVia=False)
    parser.add_argument('-via', dest='viasize', type=float, default=0.4, 
                        help="via diameter size (mm), default=0.4")
    parser.add_argument('-drill', dest='drillsize', type=float, default=0.3, 
                        help="drill diameter size (mm), default=0.3")
    parser.add_argument('-name', dest='componentname', default="Coil", 
                        help="Component name, default='Coil'")
    parser.add_argument('-N', dest='turns', type=int, default=30, 
                        help="Number of turns, default=30")

    args = parser.parse_args()
    createCoil(args.sides, args.linewidth, args.spacing, args.radius, args.centerVia, args.resolution,
               args.viasize, args.drillsize, args.componentname, args.sides, args.turns)
