# Small library for creating coils in kicad

This library is mostly a rip-off from these:  
https://github.com/joards/Simple-Planar-Coil-Generator (square coil) - only small changes  
https://gist.github.com/JoanTheSpark/e5afd7081d9d1b9ad91a (round coil) - lot of changes, most importantly creating a footprint  
None were marked with a license, but both are shared easily available. I figure they may be used freely. If any of the creators object to it, I will remove them.

I had use for both round and square coils, and gathered both of them to use them in the same way.
Both functions genereates footprints.

Tested on both python2 and python3. However, not tested at a manufacturer yet.

I may be creating a GUI at some point

Both square and round coils have been produced. Do exercise some common sense; check spacing. If you select too many turns, spacing will be sacrified. I do not take any responsibility whatsoever for these small code snippets.

Feel free to use as you please
